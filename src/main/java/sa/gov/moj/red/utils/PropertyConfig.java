package sa.gov.moj.red.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyConfig {

  private static final Logger log = LoggerFactory.getLogger(PropertyConfig.class);
  private static Properties prop;

  static {
    try (InputStream inStream = PropertyConfig.class.getClassLoader().getResourceAsStream("application.properties");) {
      log.info("static block initialized");
      if (inStream == null) {
        throw new ExceptionInInitializerError("initalization error, configuration not loaded");
      }
      prop = new Properties();
      prop.load(inStream);
    } catch (IOException ioe) {
      log.error("IOException in PopertyConfig : ", ioe);
    } catch (Exception e) {
      log.error("Exception in PopertyConfig : ", e);
    }
  }

  private PropertyConfig() {}

  public static String getProperty(final String key) {
    return prop.getProperty(key);
  }

  public static String getProperty(final String key, final String defaultValue) {
    return prop.getProperty(key, defaultValue);
  }


}
