package sa.gov.moj.red;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.json.simple.JSONObject;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.events.admin.OperationType;
import org.keycloak.events.admin.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import sa.gov.moj.red.utils.PropertyConfig;

public class RedEventListenerProvider implements EventListenerProvider {

  private static final Logger log = LoggerFactory.getLogger(RedEventListenerProvider.class);

  private final static String QUEUE_NAME = "users_queue";

  public RedEventListenerProvider() {}

  @Override
  public void onEvent(Event event) {

    log.info("Event Occurred:" + eventToJsonObject(event));

    if (event.getType().equals(EventType.LOGIN) || event.getType().equals(EventType.UPDATE_PROFILE)) {
      try {
        pushToQueue(eventToJsonObject(event).toJSONString());
      } catch (IOException e) {
        log.error("IOException while sending event", e);
      } catch (TimeoutException e) {
        log.error("TimeoutException while sending event", e);
      }
    }

  }

  public void pushToQueue(String message) throws IOException, TimeoutException {
    String host = PropertyConfig.getProperty("rabbitmq.host");
    int port = Integer.valueOf(PropertyConfig.getProperty("rabbitmq.port"));
    String userName = PropertyConfig.getProperty("rabbitmq.username");
    String password = PropertyConfig.getProperty("rabbitmq.password");
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(host);
    factory.setPort(port);
    factory.setUsername(userName);
    factory.setPassword(password);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
    log.info(" [x] Sent '" + message + "'");
    channel.close();
    connection.close();
  }

  @Override
  public void onEvent(AdminEvent adminEvent, boolean b) {
    log.info("Admin Event Occurred:" + adminEventToJsonObject(adminEvent));

    if (adminEvent.getOperationType().equals(OperationType.CREATE) && adminEvent.getResourceType().equals(ResourceType.USER)) {
      try {
        pushToQueue(adminEventToJsonObject(adminEvent).toJSONString());
      } catch (IOException e) {
        log.error("IOException while sending adminEvent", e);
      } catch (TimeoutException e) {
        log.error("TimeoutException while sending adminEvent", e);
      }
    } else if (adminEvent.getOperationType().equals(OperationType.CREATE)
        && adminEvent.getResourceType().equals(ResourceType.CLIENT_ROLE_MAPPING)) {
      try {
        pushToQueue(adminEventToJsonObject(adminEvent).toJSONString());
      } catch (IOException e) {
        log.error("IOException while sending adminEvent", e);
      } catch (TimeoutException e) {
        log.error("TimeoutException while sending adminEvent", e);
      }
    } else if (adminEvent.getOperationType().equals(OperationType.UPDATE) && adminEvent.getResourceType().equals(ResourceType.USER)) {
      try {
        pushToQueue(adminEventToJsonObject(adminEvent).toJSONString());
      } catch (IOException e) {
        log.error("IOException while sending adminEvent", e);
      } catch (TimeoutException e) {
        log.error("TimeoutException while sending adminEvent", e);
      }

    } else if (adminEvent.getOperationType().equals(OperationType.DELETE) && adminEvent.getResourceType().equals(ResourceType.USER)) {
      try {
        pushToQueue(adminEventToJsonObject(adminEvent).toJSONString());
      } catch (IOException e) {
        log.error("IOException while sending adminEvent", e);
      } catch (TimeoutException e) {
        log.error("TimeoutException while sending adminEvent", e);
      }

    }
  }

  @Override
  public void close() {}

  @SuppressWarnings("unchecked")
  private JSONObject eventToJsonObject(Event event) {
    JSONObject eventDetails = new JSONObject();
    eventDetails.put("type", event.getType());
    eventDetails.put("realmId", event.getRealmId());
    eventDetails.put("userId", event.getUserId());
    eventDetails.put("ipAddress", event.getIpAddress());
    if (event.getError() != null) {
      eventDetails.put("error", event.getError());
    }
    if (event.getDetails() != null) {
      eventDetails.put("details", event.getDetails());
    }
    return eventDetails;
  }

  @SuppressWarnings("unchecked")
  private JSONObject adminEventToJsonObject(AdminEvent adminEvent) {
    JSONObject eventDetails = new JSONObject();
    eventDetails.put("operationType", adminEvent.getOperationType());
    eventDetails.put("resourceType", adminEvent.getResourceType());
    eventDetails.put("realmId", adminEvent.getAuthDetails().getRealmId());
    eventDetails.put("clientId", adminEvent.getAuthDetails().getClientId());
    eventDetails.put("userId", adminEvent.getAuthDetails().getUserId());
    eventDetails.put("ipAddress", adminEvent.getAuthDetails().getIpAddress());
    eventDetails.put("resourcePath", adminEvent.getResourcePath());
    eventDetails.put("representation", adminEvent.getRepresentation());
    if (adminEvent.getError() != null) {
      eventDetails.put("error", adminEvent.getError());
    }
    return eventDetails;
  }



}
